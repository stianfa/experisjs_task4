const http = require('http');

const server = http.createServer((request, response) => {
    response.write('Hello Node World');
    response.end();
}).listen(8081);